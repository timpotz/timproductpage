<?php
    include "includes/autoloader.inc.php";
    require_once "templates/header.php";
    //print_r($_GET);
    //$product= new Products();
    //$product->getProducts();
    //print_r($product->getProducts());
?>

<!--Product list, add and mass delete button-->
<div class="container">
    <div class="text-left border-bottom" ><h1>Product List</h1>
        <div class="row justify-content-end">
            <div class="col-2">
                <a class="btn btn-primary" href="addProduct.php" role="button">ADD</a>
                <button id="btn_delete" class="btn btn-success" name="btn_delete" type="button">MASS DELETE</button>
                <!--<button type="button" name="btn_delete" id="btn_delete" class="btn btn-success">Delete</button>-->
        </div>
    </div>
</div>


<!--CARDS-->
<?php
include "templates/cards.php";
?>
<!--CARDS-->

<!--SCRIPTS-->
<script>
$(document).ready(function(){
 
 $('#btn_delete').click(function(){
  
   var id = [];
   
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    alert("Please Select atleast one checkbox");
   }
   else
   {
    $.ajax({
     url:'delete.process.php',
     method:'POST',
     data:{id:id},
     success:function()
     {
      for(var i=0; i<id.length; i++)
      {
       $('div#'+id[i]+'').fadeOut(0);
      }
     }
     
    });
   }
   
 
 });
 
});
</script>


<!--Scandiweb Test Assignment footer-->
<footer class=" footer fixed-bottom border-top">Scandiweb Test Assignment</footer>
    




























<?php
    require_once "templates/footer.php"
?>


























