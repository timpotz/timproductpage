<div class="container">
    <div class="row">   
        <?php $cards = new Cards();
        
            if($cards->showProducts()){
                foreach($cards->showProducts() as $card){
                    
                    if($card["productType"]=="DVD"){?>
                     <div id="<?php echo $card["productForm"]; ?>" class="col-md-4 mt-4">
                         <div class="card-body border text-center">             
                         <input type="checkbox" name="product_id[]" class="delete_product delete-checkbox row justify-content-left" value="<?php echo $card["productForm"]; ?>">
                            <h5 class="card-title text-center"><?=$card ["SKU"]?></h5>
                                <p class="card-text"><?=$card ["name"]?></p>
                                <p class="card-text">$<?=$card ["price"]?></p>
                                <p class="card-text"><?=$card ["size"]?> MB</p>
                                
                          </div>
                      </div>
                    <?php }
                    elseif($card["productType"]=="Book"){?>
                        <div id="<?php echo $card["productForm"]; ?>" class="col-md-4 mt-4">
                            <div class="card-body border text-center">
                            <input type="checkbox" name="product_id[]" class="delete_product delete-checkbox row justify-content-left" value="<?php echo $card["productForm"]; ?>">

                                <h5 class="card-title text-center"><?=$card ["SKU"]?></h5>
                                    <p class="card-text"><?=$card ["name"]?></p>
                                    <p class="card-text">$<?=$card ["price"]?></p>
                                    <p class="card-text"><?=$card ["weight"]?>KG</p>
                                  
                            </div>
                        </div>
                <?php    }
                   elseif($card["productType"]=="Furniture"){?>
                    <div id="<?php echo $card["productForm"]; ?>" class="col-md-4 mt-4">
                        <div class="card-body border text-center">
                        <input type="checkbox" name="product_id[]" class="delete_product delete-checkbox row justify-content-left" value="<?php echo $card["productForm"]; ?>">
                            <h5 class="card-title text-center"><?=$card ["SKU"]?></h5>
                                <p class="card-text"><?=$card ["name"]?></p>
                                <p class="card-text">$<?=$card ["price"]?></p>
                                <p class="card-text"><?=$card ["height"]."cm"." x ".$card ["width"]."cm"." x ".$card ["length"]."cm"?></p>
                               
                        </div>
                    </div>
                <?php    }
                    else{?>
                                <p class="card-text">There are no products available</p>
                    <?php }}
            }?>
                
                        </div>
                    </div>
    </div>
</div>       
