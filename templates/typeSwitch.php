
<div class="container">
    <div class="row justify-content-left">
        <div class="col col-lg-3">
            <form id="product_form" action="add.process.php" method="POST">
            <div class="alert alert-danger" role="alert" id="message" style="display: none;">
                
            </div>
            <div class="form-group">
                <label for="sku">SKU</label>
                <input id="sku" type="text" class="form-control" name="sku" required>
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input id="name" type="text" class="form-control" name="name" required>
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                <input id="price" type="number" step="0.01" class="form-control" name="price" required>
            </div>

            
            <br>
            <!--<div class="form-group">
                <input id="productType" type="radio" onclick="Form()" name="productType" value="DVD">DVD
                <input id="productType" type="radio" onclick="Form()" name="productType" value="Book">Book
                <input id="productType" type="radio" onclick="Form()" name="productType" value="Furniture">Furniture
            </div>-->
            <div class="form-group">
            <p>Please Select Product</p>
            <select name="productType" id="productType">
            <option value="" disabled selected>Please select Product</option>
                <option value="DVD">DVD</option>
                <option value="Book">Book</option>
                <option value="Furniture">Furniture</option>
            </select>
            </div>

            <div id="prdType">
               
            </div>
            
           
            <br>
            <input style="visibility:hidden" id="save" type="submit" name="submit" class="btn btn-primary" value="Save">
            </form>

            <script>
    $(document).ready(function(){
        $("#productType").change(function(){
            var value=$(this).val();
            switch(value){
                case "DVD":
                    $("#prdType").load("forms/DVD.php");
                    break;
                case "Book":
                    $("#prdType").load("forms/Book.php");
                    break;
                case "Furniture":
                    $("#prdType").load("forms/Furniture.php");
                    break;
                default:
                    $('#prdType').html("Please Select a Product");
            }
        })
    })
</script>
            <br>
</div>