<?php

class FurnitureContr extends Furniture {

    protected $sku;
    protected $name;
    protected $price;
    protected $productType;
    protected $height;
    protected $width;
    protected $length;

    public function __construct($products){
        foreach($products as $product=>$value){
        $this->$product=$value;
    }
    }

    public function add(){
    $this->saveProduct($this->sku,$this->name,$this->price,$this->productType,$this->height,$this->width,$this->length);
    }
}