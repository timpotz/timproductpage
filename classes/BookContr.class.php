<?php

class BookContr extends Book{
    //PROPERTIES
    protected $sku;
    protected $name;
    protected $price;
    protected $productType;
    protected $weight;

    public function __construct($products){
        foreach($products as $product=>$value){
        $this->$product=$value;
        }
    }

    //METHODS
    public function add(){
        $this->saveProduct($this->sku,$this->name,$this->price,$this->productType,$this->weight);
    }
}