<div class="form-group" id="attributes">
    <br>
                        <p>
                        Please insert the dimension (Height x Width x Length) of your furniture
                        </p>
                        <label for="height">Height (cm)</label>
                        <input id='height' type="number" class="form-control" name="height" required> 
                        <label for="width">Width (cm)</label>
                        <input id='width' type="number" class="form-control" name="width" required> 
                        <label for="length">Length (cm)</label>
                        <input id='length' type="number" class="form-control" name="length" required>
</div>